This repository serve as notes for me.

It contains:

- journey.txt	: Trip report
- memo.txt	: Story about My Jobs

In Bahasa Indonesia

It encrypted using my key [1].

To decrypt:

`gpg --decrypt file.txt.asc > file.txt`

To encrypt:

`gpg --default-recipient-self --armor --encrypt file.txt`


[1] 1C0AE7DBA1819072989123EA20006FC222B23A62
